package fr.ulille.iut.pizzaland.resources;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import java.net.URI;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;




@Path("pizzas")
public class PizzaResource {
    private static final Logger LOGGER = Logger.getLogger(PizzaResource.class.getName());
    private PizzaDao pizzas;


    @Context
    public UriInfo uriInfo;

    public PizzaResource() {
        pizzas = BDDFactory.buildDao(PizzaDao.class);
        pizzas.createTableAndIngredientAssociation();
    }
    

    @GET
    public List<PizzaDto> getAll() {
        LOGGER.info("PizzaResource:getAll");

	return null;
    }

    @GET
    @Path("{id}")
    public PizzaDto getOnePizza(@PathParam("id") long id) {
          LOGGER.info("getOnePizza(" + id + ")");
          try {
              Pizza pizza = pizzas.findById(id);
              return Pizza.toDto(pizza);
          }
          catch ( Exception e ) {
              // Cette exception générera une réponse avec une erreur 404
              throw new WebApplicationException(Response.Status.NOT_FOUND);
          }
    }

    @POST
    public Response createPizza(PizzaCreateDto pizzaCreateDto) {
        Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
        if ( existing != null ) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }
        
        try {
            Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
            long id = pizzas.insert(pizza.getName());
            pizza.setId(id);
            PizzaDto pizzaDto = Pizza.toDto(pizza);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

            return Response.created(uri).entity(pizzaDto).build();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }
  
}